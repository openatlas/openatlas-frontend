class Changelog:
    versions = {
        '0.2.0': ['TBA', {
            'feature': {},
            'fix': {}}],
        '0.1.0': ['2020-05-15', {
            'feature': {
                '1242': 'Initial Flask framework'}}]}
