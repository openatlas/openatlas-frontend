# About

OpenAtlas frontend is a presentation website for [OpenAtlas](https://openatlas.eu) projects.

At the moment we are in the concept phase and this is just a basic start point for further development.

# Licensing

All OpenAtlas frontend code unless otherwise noted is licensed under the terms of the GNU General Public License Version 2,
June 1991. Please refer to the file COPYING in the root directory of this repository or the online version at https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
